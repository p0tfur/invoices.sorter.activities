## Invoices Sorter Activities
This package can be used to build your own sorting robot for any type of documents.

####Activities set:

*  Analyze PDF, analyzes provided string, recognizing the entity by VAT registration number or entity name and using collocations (bigrams - most commonly (two) adjacent words e.g. invoice date) assess the document type.
*  Count Attachments, suppliers/customers use various mailing clients/systems and unfortuantely there is not any standard regarding to marking inline/embedded pictures. So in a lot of cases Attachments.Count != count of real attachments. This really disturbs invoicing sorting process, because you have to know, is it company logo from footer or invoice sent as image instead of PDF file? This activity does the job for you, disinguishing real attachments from inline ones. Additionally, you can use filter option to do not treat some types of attachments (basing on name or extension) as real attachments i.e. some companies attach .vcf file to each e-mail with invoice. This is your trading partner, you really do not need see his business card everytime, right?
This activity also takes care about attached messages (.eml, .msg files). Counts attachments attached to them and if all are PDF files saves them in provided temporary directory, from where can be processed further.
*  Save Attachment By Index, activity based on Microsoft Exchange Web Services (EWS). Analyze PDF activity returns indexes of real attachments, which can be saved on drive for further processing using this activity.
*  Add String To List, as Robot operates on lists a lot, this activity helps to add provided string to list.
*  Create Drive Directories, this activity basing on input datatable creates directories on drive. 

[See also example Invoices Sorter Robot](https://bitbucket.org/p0tfur/invoices.sorter/) build using these activities.
