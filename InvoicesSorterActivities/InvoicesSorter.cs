﻿using System;
using System.Activities;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Microsoft.Exchange.WebServices.Data;
using System.IO;
using System.Data;

//make sure you validate all user inputs. Validate, validate, validate!
//give some feedback if execution is successfull(additional output field with <string>info) or not
//inputs, spearate groups for input/required/optional/output
//add dependencies as nuget package dependecies (not content)
namespace Invoices_Sorter_Activities
{
    public class Analyze_PDF : CodeActivity
    {
        [Category("Input")]
        [DisplayName("PDF Text")]
        [Description("PDF text as string, for better results you should use 'Prepare String To Analyze' and 'Remove Stopwords' activities before. ")]
        [RequiredArgument]
        public InArgument<string> PdfText { get; set; }

        [Category("Input")]
        [DisplayName("Invoice collocations")]
        [Description("Datatable with collocations for invoice")]
        [RequiredArgument]
        public InArgument<DataTable> DtInvoice { get; set; }

        [Category("Input")]
        [DisplayName("Non-Invoice collocations")]
        [Description("Datatable with collocations for other documents")]
        [RequiredArgument]
        public InArgument<DataTable> DtOther { get; set; }

        [Category("Input")]
        [DisplayName("Datatable entities")]
        [Description("Datatable with VAT number and corresponding entities names.")]
        [RequiredArgument]
        public InArgument<DataTable> DtEntities { get; set; }

        [Category("Output")]
        [DisplayName("Invoice")]
        [Description("Return true if document has been recognized as invoice. False for all other documents.")]
        [RequiredArgument]
        public OutArgument<bool> Result { get; set; }

        [Category("Output")]
        [DisplayName("Directory name")]
        [Description("Name of directory where attachments/message has to be moved if hasn't to be checked by human.")]
        public OutArgument<string> Dir { get; set; }

        [Category("Output")]
        [DisplayName("Scoring")]
        [Description("Scoring is autocalculated and activity decides about document type, but basing on this output you can decide about classification on your own.")]
        public OutArgument<int> Scoring { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            //get data from user
            string pdfText = PdfText.Get(context);
            DataTable dt = DtEntities.Get(context);
            DataTable dtInvoice = DtInvoice.Get(context);
            DataTable dtOther = DtOther.Get(context);

            //if datatables empty, throw exception
            if (dt.Rows.Count == 0 || dtInvoice.Rows.Count == 0 || dtOther.Rows.Count == 0)
            {
                throw new Exception("Datatable entities/collocations is empty!");
            }

            //scoring, used to determine that this is invoice or not
            int scoring = 0;
            int scoringOther = 0;
            string docDirectory = null;
            bool result = false;
            //text should be already prepared for processing by Prepare String To Analyze and Remove Stopwords activites

            //## entity recognition    
            string pattern2 = Regex.Replace(pdfText, @"[^a-zA-Z\s]", ""); //leave only letters and whitespaces, as whitelist is always(?) better than blacklist
            string pattern1 = Regex.Replace(pdfText, @"[^a-zA-Z]", ""); //leave only letters

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dt.Rows[i];

                //match VAT reg. no.   
                string vat = dr[0].ToString().ToLower();
                if (vat != "")
                {
                    if (Regex.IsMatch(pdfText, Regex.Replace(vat, pattern1, ""))) //prepare VAT for matching, in case wrongly provided in config file
                    {
                        scoring += 10; //vat found, it is common practice that documents other than invoice has only name, without VAT, so points only for invoice here
                        if (dr[2].ToString() != "")
                        {
                            docDirectory = dr[2].ToString();
                            break; //match found, prevent to cumulate score if VAT stated a few times
                        }
                        else
                        {
                            //we cannot process without complete config file!
                            throw new Exception("Directory name is empty, correct config file!");
                        }

                    }

                }

                //match company name
                string name = dr[1].ToString().ToLower();
                if (name != "")
                {
                    if (Regex.IsMatch(pdfText, Regex.Replace(name, pattern2, "")))
                    {
                        scoring += 5; //if there is not VAT, only names suggest for what entity this document is - but this has not to be invoice
                        scoringOther += 5; //score for both
                        if (docDirectory == null)
                        {
                            if (dr[2].ToString() != "")
                            {
                                docDirectory = dr[2].ToString();
                                break; //match found, prevent to cumulate score if name stated a few times
                            }
                            else
                            {
                                throw new Exception("Directory name is empty, correct config file!");
                            }
                        }
                    }
                }
            }

            if (scoring != 0) //correctly addressed, process it further
            {
                //check collocations and decide, invoice or not?

                for (int i = 0; i < dtInvoice.Rows.Count; i++)
                {
                    DataRow dr = dtInvoice.Rows[i];

                    //match invoice collocations  
                    string collocation = dr[0].ToString();
                    if (collocation != "")
                    {
                        MatchCollection matches = Regex.Matches(pdfText, collocation);
                        if (matches.Count > 0)
                        {
                            if (Int32.TryParse(dr[1].ToString(), out int score))
                            {
                                scoring += matches.Count * score;
                            }
                            else
                            {
                                //Console.WriteLine("Parsing failed {0}: ", dr[1].ToString());
                            }

                        }
                    }
                }

                //match NON-invoice collocations  
                for (int i = 0; i < dtOther.Rows.Count; i++)
                {
                    DataRow dr = dtOther.Rows[i];

                    //match NON-invoice collocations  
                    string collocation = dr[0].ToString();
                    if (collocation != "")
                    {
                        MatchCollection matches = Regex.Matches(pdfText, collocation);
                        if (matches.Count > 0)
                        {
                            if (Int32.TryParse(dr[1].ToString(), out int score))
                            {
                                scoringOther += matches.Count * score;
                            }
                            else
                            {
                                //Console.WriteLine("NON Parsing failed {0}: ", dr[1].ToString());
                            }

                        }
                    }
                }

                //evaluate scoring
                if (scoring > scoringOther)
                {
                    result = true;
                }
                //else use default false

                //TODO: tokenization + word frequency analyse
                //TODO: word position analyse
            }
            else //entity not recognized, by default goes to manual check
            {
                //scoring 0 means that this has to be checked by human
                //nothing to do here
            }

            //return values
            Dir.Set(context, docDirectory);
            Result.Set(context, result);
            Scoring.Set(context, scoring);
        }
    }

    public class Add_String_To_List : CodeActivity
    {
        [Category("Input")]
        [DisplayName("String")]
        [Description("String which you want add to list")]
        [RequiredArgument]
        public InArgument<string> Item { get; set; }

        [Category("Input")]
        [DisplayName("Existing list")]
        [Description("Existing list of strings")]
        [RequiredArgument]
        public InArgument<List<string>> List { get; set; }

        [Category("Output")]
        [DisplayName("Updated list")]
        [Description("Updated list of strings")]
        [RequiredArgument]
        public OutArgument<List<string>> ListNew { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            string item = Item.Get(context);
            List<string> list = List.Get(context);
            list.Add(item);
            ListNew.Set(context, list);
        }
    }

    public class Create_Drive_Directories : CodeActivity
    {
        [Category("Input")]
        [DisplayName("Path")]
        [Description("Drive path, where directories will be created")]
        [RequiredArgument]
        public InArgument<string> DrivePath { get; set; }

        [Category("Input")]
        [DisplayName("Directories")]
        [Description("Single column DataTable of directories to be created")]
        [RequiredArgument]
        public InArgument<DataTable> DirsDT { get; set; }

        [Category("Output")]
        [DisplayName("Exception")]
        [Description("In case of exception, it will be returned here so you can use/process it further.")]
        public OutArgument<Exception> Except { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            string path = DrivePath.Get(context);
            System.Data.DataTable dirsDT = DirsDT.Get(context);

            //put data from dt column 0 to list
            var dirsList = dirsDT.AsEnumerable().Select(r => r.Field<string>(0)).ToList();

            try
            {
                //iterate through whole list
                foreach (string dir in dirsList)
                {
                    if (!Directory.Exists(@path + @dir))
                    {
                        DirectoryInfo di = Directory.CreateDirectory(@path + @dir);
                    }
                    else
                    {
                        Console.WriteLine("Create_Drive_Directories: Directory exists already {0}{1}: ", @path, @dir);
                    }
                }
            }
            catch(Exception e)
            {
                Console.WriteLine("Create_Drive_Directories: Creating directory failed: {0}", e.ToString());
                Except.Set(context, e);
            }
            
        }
    }

    /*instead of use UiPath activity where full range can be read
    public class Read_PDF_To_Text : CodeActivity
    {
        [Category("Input")]
        [DisplayName("PDF path")]
        [Description("Path to .pdf file")]
        [RequiredArgument]
        public InArgument<string> PdfPath { get; set; }

        [Category("Input")]
        [DisplayName("Pages")]
        [Description("Amount of pages to be extracted, for all pages type in 0. No more than 10 pages can be read.")]
        [RequiredArgument]
        public InArgument<int> PdfPages{ get; set; }

        [Category("Output")]
        [DisplayName("PDF Text")]
        [Description("Return .pdf file text as string")]
        [RequiredArgument]
        public OutArgument<string> PdfText { get; set; }

        [Category("Output")]
        [DisplayName("Protected")]
        [Description("Return true if file is password protected")]
        public OutArgument<bool> Protected { get; set; }

        //def value
        public Read_PDF_To_Text()
        {
            this.PdfPages = new InArgument<int>(0);
        }

        protected override void Execute(CodeActivityContext context)
        {
            string pdfPath = PdfPath.Get(context);
            int pdfPagesToRead = PdfPages.Get(context);
            bool passwordProt = false;
            string pdfText = null;

            if (!pdfPath.ToLower().EndsWith(".pdf"))
            {
                Console.WriteLine("Read_PDF_To_Text: File type not suported {0}", Path.GetExtension(pdfPath));
            }
            else
            {
                try
                {
                    //load pdf document
                    PdfDocument document = new PdfDocument();
                    document.LoadFromFile(pdfPath);
                    //extract text
                    StringBuilder pdfContent = new StringBuilder();;
   
                    if (pdfPagesToRead == 0)
                    {
                        //read all pages
                        foreach (PdfPageBase page in document.Pages)
                        {
                            pdfContent.Append(page.ExtractText());
                        }   
                    }
                    else
                    {
                        //extract text from x pages
                        for (int i = 0; i < pdfPagesToRead; i++)
                        {
                            try
                            {
                                pdfContent.Append(document.Pages[i].ExtractText());
                            }
                            catch { } //prevent error when user wants read more pages than file has
                        }
                    }

                    //clean string
                    pdfText = pdfContent.ToString();
                    pdfText = pdfText.Replace(@"Evaluation Warning : The document was created with Spire.PDF for .NET.", ""); //it is free version without limitations, why is it here? It shouldn;t be here - comfirmed by e-iceblue.com
                    pdfText = pdfText.Replace(@":", " ");
                    pdfText = Regex.Replace(pdfText, @"\r\n", " ");
                    pdfText = Regex.Replace(pdfText, @"[ ]{2,}", " ");  
                }
                catch(Exception e)
                {
                    if (e.ToString().ToLower().Contains("password is invalid") || e.ToString().ToLower().Contains("password is incorrect"))
                    {
                        Console.WriteLine("Read_PDF_To_Text: cannot read password protected file");
                        passwordProt = true;
                    }
                    else
                    {
                        //other exception
                        Console.WriteLine("Read_PDF_To_Text: error when reading file {0}", e);
                    }
                }

                //return values
                PdfText.Set(context, pdfText);
                Protected.Set(context, passwordProt); 
            }
        }
    }
    */

    public class Count_Attachments : CodeActivity 
    {
        [Category("Input")]
        [DisplayName("EmailMessage")]
        [Description("EWS EmailMessage")]
        [RequiredArgument]
        public InArgument<EmailMessage> Mail { get; set; }

        [Category("Input")]
        [DisplayName("Temp path")]
        [Description("Path to temporary save files if needed (when another message is attached to email)")]
        [RequiredArgument]
        public InArgument<string> TempPath { get; set; }

        [Category("Optional")]
        [Description("Extensions or file names you want to NOT treat as real/valid attachments, separate file/extensions by | i.e: 'bin|xml|vcf|ics|index.html'. For checking lowercase string is used.")]
        [DisplayName("Omit")]
        public InArgument<string> Omit { get; set; }

        [Category("Output")]
        [DisplayName("List PDF index")]
        [Description("List with indexes of attachments containing PDF files")] 
        [RequiredArgument]
        public OutArgument<List<int>> ListPdfIndex { get; set; }

        [Category("Output")]
        [DisplayName("List EML attachments")]
        [Description("List with names of PDF attachments, attached in message, which is attached to main message")]
        [RequiredArgument]
        public OutArgument<List<string>> ListEml{ get; set; }

        [Category("Output")]
        [DisplayName("Manual Check")]
        [Description("Should this e-mail be checked by human being?")]
        [RequiredArgument]
        public OutArgument<bool> ManualCheck { get; set; }

        //default values
        public Count_Attachments()
        {
            this.Omit = new InArgument<string>("bin|xml|ged|ads|att00001.htm|att00002.htm|att00001.txt|att00002.txt|att00001|att00002");
            this.TempPath = @"C:\Users\Public\Documents\";
        }

        protected override void Execute(CodeActivityContext context)
        {
            string tempPath = TempPath.Get(context);

            //to retrieve message HTML body 
            PropertySet itempropertyset = new PropertySet(BasePropertySet.FirstClassProperties);
            itempropertyset.RequestedBodyType = BodyType.HTML;

            EmailMessage msg = Mail.Get(context);
            msg.Load(itempropertyset);

            //read message body
            string msgBody = msg.Body;
            //Console.WriteLine("msgBody: {0}", msgBody);

            //counters to check how many all/valid/pdf attachments there are
            int validCounter = 0;
            int pdfCounter = 0;
            int totalCounter = msg.Attachments.Count;
            //list to store indexes of PDF attachments, so these can be retrieved later
            List<int> listPdfIndex = new List<int>();
            //list to store paths to attachments from attached messages
            List<string> listEml = new List<string>();
            //should this mail be checked by human?
            bool manualCheck = false;

            //omit string to list
            List<string> fileOmit = Omit.Get(context).Split('|').ToList();

            if (msg.HasAttachments)
            {
                //index to identify attachment
                int i = -1;
                foreach (Attachment attach in msg.Attachments)
                {
                    //increase index, so at start it is 0
                    i++;

                    //cid: occurs in message text before inline/embedded images, so these are not valid/real attachments
                    if (msgBody.Contains("cid:" + attach.Name))
                    {
                        totalCounter--;
                    }
                    else
                    {
                        if (attach is FileAttachment) //file
                        {
                            FileAttachment fileAttachment = attach as FileAttachment;
                            fileAttachment.Load();

                            //file attachment name in lowercase
                            string attNameLower = fileAttachment.Name.ToLower();
                            if (Path.GetExtension(attNameLower) == ".pdf")
                            {
                                validCounter++;
                                pdfCounter++;
                                //add index of pdf attachment to list - ?alternativly this can be saved?
                                listPdfIndex.Add(i);
                            }
                            //check if extension of file is on the list
                            else if (fileOmit.Any(Path.GetExtension(attNameLower).Contains) == true)
                            {
                                totalCounter--;
                            }
                            //check if file name is on the list
                            else if(fileOmit.Any(attNameLower.Contains) == true)
                            {
                                totalCounter--;
                            }
                            //here handle .eml files, as these are not treated as ItemAttachment
                            else if (Path.GetExtension(attNameLower) == ".eml")
                            {
                                //save message on drive
                                string filePath = SaveFile(attach, tempPath);
                                //do the counting
                                var countMess = CountEmlMsg(attach, new FileInfo(@filePath), manualCheck, msgBody, tempPath, listEml, fileOmit, totalCounter, validCounter, pdfCounter);
                                //assign values returned from Tuple class
                                totalCounter = countMess.Item1;
                                validCounter = countMess.Item2;
                                pdfCounter = countMess.Item3;
                                manualCheck = countMess.Item4;
                            }
                            else //not pdf
                            {
                                validCounter++;
                                //remove quotes to eliminate issues with matching
                                msgBody = msgBody.Replace("\"", "");
                                //possible extensions for images
                                string[] extensions = { ".jpeg", ".jpg", ".jfif", ".exif", ".gif", ".bmp", ".tiff", ".png", ".ppm", ".pgm", ".pbm", ".pnm", ".webp", ".gdraw", ".bpg", ".drw", ".heif" };
                                //linq to check if attachment name is image file - extensions above
                                bool checkExt = extensions.Any(x => attach.Name.ToLower().EndsWith(x));

                                //not all -email clients follow practice that embedded/inline image is preceded by 'cid:'
                                //so some workaround needed, if attachment name is in body precceded by <img> tag, it is inline image, this is reliable in 99%
                                if (msgBody.Contains("<img") && checkExt == true)
                                {
                                    totalCounter--;
                                    validCounter--;
                                }
                            }
                        }
                        else if (attach is ItemAttachment) //email's and other
                        {
                            ItemAttachment itemAttachment = attach as ItemAttachment;
                            itemAttachment.Load(EmailMessageSchema.MimeContent);

                            if (itemAttachment.Item is EmailMessage) //.msg, not works for .eml
                            {
                                //read attachments and do the same what for 'normal' attachments

                                //save message on drive
                                string filePath = SaveFile(attach, tempPath);
                                //do the counting
                                var countMess = CountEmlMsg(attach, new FileInfo(@filePath), manualCheck, msgBody, tempPath, listEml, fileOmit, totalCounter, validCounter, pdfCounter);
                                //assign values returned from Tuple class
                                totalCounter = countMess.Item1;
                                validCounter = countMess.Item2;
                                pdfCounter = countMess.Item3;
                                manualCheck = countMess.Item4;
                            }
                            //contact/appointment attached - we shouldn't be here as this is .vcf/.ics, but just in case, don't treat these as real attachments
                            else if (itemAttachment.Item is Contact || itemAttachment.Item is Appointment)
                            {
                                totalCounter--;
                            }
                            else //other
                            {
                                //other, let human check it
                                validCounter++;
                            }
                        }
                    }
                }
            }
            else
            {
                //assign to manual check
                manualCheck = true;
            }

            //there are not any valid/pdf attachments
            if(validCounter == 0 || pdfCounter == 0)
            {
                //this to be checked by human
                manualCheck = true;
                ManualCheck.Set(context, manualCheck);
            }
            else if(pdfCounter == validCounter && pdfCounter != 0)
            {
                //this to be processed
                manualCheck = false;
            }
            else //this to be checked by human
            {
                manualCheck = true;
                ManualCheck.Set(context, manualCheck);
            }
            
            //return values
            ListPdfIndex.Set(context, listPdfIndex);
            ManualCheck.Set(context, manualCheck);
            ListEml.Set(context, listEml);
        }

        public Tuple<int, int, int, bool> CountEmlMsg(Attachment attach, FileInfo fileInfo, bool manualCheck, string msgBody, string tempPath, List<string> listEml, List<string> fileOmit, int totalCounter, int validCounter, int pdfCounter)
        {
            //create message
            var eml = MsgReader.Mime.Message.Load(fileInfo);

            if (eml.Attachments.Count > 0)
            {
                //update total - attachments from this message have to be included also
                totalCounter = totalCounter + eml.Attachments.Count - 1; //-1 for this message
                //iterate through all attachments
                foreach (MsgReader.Mime.MessagePart attach2 in eml.Attachments)
                {
                    string attNameLower = attach2.FileName.ToLower(); //attachment name in lowercase

                    //cid: occurs in message text before inline/embedded images, so these are not valid/real attachments
                    if (msgBody.Contains("cid:" + attach2.FileName))
                    {
                        totalCounter--;
                    }
                    else if (Path.GetExtension(attNameLower) == ".pdf")
                    {
                        validCounter++;
                        pdfCounter++;

                        //save pdf file
                        try
                        {
                            string saveName = Path.GetRandomFileName().Replace(".", string.Empty) + Path.GetExtension(attNameLower);
                            byte[] attachBytes = attach2.Body;
                            FileStream attachStream = File.Create(@tempPath + saveName);
                            attachStream.Write(attachBytes, 0, attachBytes.Length);
                            attachStream.Close();

                            //add file path to list
                            listEml.Add(saveName);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("Count Attachments: cannot save file {0}", e);
                        }
                    }
                    //check if extension of file is on the list to omit
                    else if (fileOmit.Any(Path.GetExtension(attNameLower).Contains) == true)
                    {
                        totalCounter--;
                    }
                    //check if file name is on the list to omit
                    else if (fileOmit.Any(attNameLower.Contains) == true)
                    {
                        totalCounter--;
                    }
                    else //other files
                    {
                        validCounter++; //as this is (probably) real attachment
                        //remove quotes to eliminate issues with matching
                        msgBody = msgBody.Replace("\"", "");
                        //possible extensions for inline images
                        string[] extensions = { ".jpeg", ".jpg", ".jfif", ".exif", ".gif", ".bmp", ".tiff", ".png", ".ppm", ".pgm", ".pbm", ".pnm", ".webp", ".gdraw", ".bpg", ".drw", ".heif" };
                        //linq to check if attachment name is image file using extensions array
                        bool checkExt = extensions.Any(x => attach.Name.ToLower().EndsWith(x));

                        //not all -email clients follow practice that embedded/inline image is preceded by 'cid:'
                        //so some workaround needed, if attachment name is in body preceded by <img> tag it is inline image, this is reliable in 99%
                        if (msgBody.Contains("<img") && checkExt == true)
                        {
                            totalCounter--;
                            validCounter--;
                        }
                    }
                }
            }
            else
            {
                //no attachments
                manualCheck = true;
            }

            //done delete .eml file, not needed anymore
            string fileName = @fileInfo.FullName.ToString();
            if (File.Exists(fileName))
            {
                try
                {
                    File.Delete(fileName);
                }
                catch(Exception e)
                {
                    Console.WriteLine(string.Format("Cannot delete the file {0}", fileName), e);
                }
                
            }


            return Tuple.Create(totalCounter, validCounter, pdfCounter, manualCheck);
        }

        public string SaveFile(Attachment attach, string path)
        {
            if (attach is FileAttachment) //.eml
            {
                FileAttachment fileAttachment = attach as FileAttachment;
                fileAttachment.Load();

                //generate random name for file
                string saveName = Path.GetRandomFileName().Replace(".", string.Empty) + Path.GetExtension(fileAttachment.Name);

                fileAttachment.Load(@path + saveName);

                return @path + saveName;
            }
            else if (attach is ItemAttachment) //.msg
            {
                ItemAttachment itemAttachment = attach as ItemAttachment;
                itemAttachment.Load(EmailMessageSchema.MimeContent);

                //by default ItemAttachment has not extension, save it as .eml, however it is .msg
                string fileExtension = ".eml";
                //generate random file name
                string saveName = Path.GetRandomFileName().Replace(".", string.Empty);
                ///save
                File.WriteAllBytes(@path + saveName + fileExtension, itemAttachment.Item.MimeContent.Content);

                return @path + saveName + fileExtension;
            }
            else
            {
                return "SaveFile: Incorrect type of attachment provided, cannot process";
            }
        }

    }

    //developer activity, not to be included in official release
    //for next versions, develop towards ML?
    /*
    public class Calibrate_Sorter : CodeActivity
    {
        //entity recognition, tokenization, Stopword Removal
        //collocations for keywords
        //concordance
        [Category("Input")]
        [DisplayName("Directory")]
        [Description("Directory where pdf's are stored, full path")]
        [RequiredArgument]
        public InArgument<string> DirPath { get; set; }

        [Category("Output")]
        [DisplayName("DataTable Words")]
        [Description("Datatable with words, occurences count")]
        [RequiredArgument]
        public OutArgument<System.Data.DataTable> WordsData { get; set; }

        [Category("Output")]
        [DisplayName("DataTable Positions")]
        [Description("Datatable with words and positions.")]
        [RequiredArgument]
        public OutArgument<System.Data.DataTable> WordsPositions { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            //get data  from user
            string dirPath = DirPath.Get(context);

            //create new datatable, for words count
            System.Data.DataTable dt = new System.Data.DataTable();
            dt.Columns.Add("Word", typeof(string));
            dt.Columns.Add("Count", typeof(Int32));
            //dt.Columns.Add("Position");
            //create new datatable for words positions
            System.Data.DataTable dtPositions = new System.Data.DataTable();
            dtPositions.Columns.Add("Word", typeof(string));
            dtPositions.Columns.Add("Position", typeof(Int32));

            foreach (string pdfPath in Directory.GetFiles(dirPath))
            {
                if (!pdfPath.ToLower().EndsWith(".pdf"))
                {
                    Console.WriteLine("Calibrate_Sorter: File type not suported {0}", Path.GetExtension(pdfPath));
                }
                else
                {

                    //load pdf document
                    PdfDocument document = new PdfDocument();
                    document.LoadFromFile(pdfPath);
                    //extract text
                    StringBuilder pdfContent = new StringBuilder();

                    //read all pages - up to 10
                    foreach (PdfPageBase page in document.Pages)
                    {
                            pdfContent.Append(page.ExtractText());
                    }

                    //clean string
                    string pdfText = pdfContent.ToString(); 
                    pdfText = pdfText.Replace(@"Evaluation Warning : The document was created with Spire.PDF for .NET.", ""); //it is free version for commercial use, so why is it here?
                    //pdfText = Regex.Replace(pdfText, @"[:!@#$%^&*\-()}{|:?><\[\]\\;'/.,~`’\d]", ""); //remove special chars and digits
                    pdfText = Regex.Replace(pdfText, @"[^a-zA-Z\s]", ""); //leave only letters, as white list is always better than blacklist
                    pdfText = pdfText.Replace("\"", "");
                    pdfText = Regex.Replace(pdfText, @"\r\n", " ");
                    pdfText = Regex.Replace(pdfText, @"[ ]{2,}", " ");
 
                    //lowercase
                    pdfText = pdfText.ToLower();
                    //Console.WriteLine("Length: {0}", pdfText.Length);

                    //put document text into array
                    string[] words = pdfText.Split(new char[0], StringSplitOptions.RemoveEmptyEntries);

                    //remove duplicates
                    string[] wordsWithoutDuplicates = words.Distinct().ToArray();

                    //stopwords English
                    string[] stopEng = new string[] { "a", "about", "above", "after", "again", "against", "all", "am", "an", "and", "any", "are", "arent", "as", "at", "be", "because", "been", "before", "being", "below", "between", "both", "but", "by", "cant", "cannot", "could", "couldnt", "did", "didnt", "do", "does", "doesnt", "doing", "dont", "down", "during", "each", "few", "for", "from", "further", "had", "hadnt", "has", "hasnt", "have", "havent", "having", "he", "hed", "hell", "hes", "her", "here", "heres", "hers", "herself", "him", "himself", "his", "how", "hows", "i", "id", "ill", "im", "ive", "if", "in", "into", "is", "isnt", "it", "its", "its", "itself", "lets", "me", "more", "most", "mustnt", "my", "myself", "no", "nor", "not", "of", "off", "on", "once", "only", "or", "other", "ought", "our", "ours", "ourselves", "out", "over", "own", "same", "shant", "she", "shed", "shell", "shes", "should", "shouldnt", "so", "some", "such", "than", "that", "thats", "the", "their", "theirs", "them", "themselves", "then", "there", "theres", "these", "they", "theyd", "theyll", "theyre", "theyve", "this", "those", "through", "to", "too", "under", "until", "up", "very", "was", "wasnt", "we", "wed", "well", "were", "weve", "were", "werent", "what", "whats", "when", "whens", "where", "wheres", "which", "while", "who", "whos", "whom", "why", "whys", "with", "wont", "would", "wouldnt", "you", "youd", "youll", "youre", "youve", "your", "yours", "yourself", "yourselves" };

                    //iterate through array and add words and occurences count
                    foreach (string word in wordsWithoutDuplicates)
                    {
                        
                       //check if word is in dt already
                        DataRow row = dt.Select("Word=" + "'" + word + "'").FirstOrDefault();
                        if (row == null)
                        {
                            //do not add if stopword
                            if(Array.IndexOf(stopEng, word) >= 0)
                            {
                                //Console.WriteLine("#------STOPWORD: {0}", word);
                            }
                            else if(word.Count() == 1)
                            {
                                //Console.WriteLine("#####OMIT: {0}", word);
                            }
                            else
                            {
                                //add, not in dt already, not stopword
                                dt.Rows.Add(word, CountStringOccurr(words, word));
                            }
                        }
                        else
                        {
                            //in dt already, just increase counter
                            row["Count"] = Convert.ToInt32(row["Count"]) + 1;
                        }
                    }

                    //iterate through full array and record postition
                    int lastPosition = 0;
                    foreach (string word in words)
                    {
                        int position = pdfText.IndexOf(word, lastPosition);

                        if (word.Count() == 1)
                        {
                            //Console.WriteLine("#####OMIT: {0}", word);
                        }
                        else if(position != -1) //not not found
                        {
                            lastPosition = position;
                            //here we always add row to datatable, later we should iterate through it and count average position
                            dtPositions.Rows.Add(word.ToString(), position);
                        }
                        if(word == "invoice")
                        {
                            //Console.WriteLine("Invoice pos: {0}", position);
                        }
                    }
                }
            }
            //find duplicates in dtPositions, count average postitions and leave only 1 occurence
            var duplicates = dtPositions.AsEnumerable()
                .GroupBy(row => row.Field<string>("Word"))
                .Where(g => g.Count() > 1);

            foreach(var group in duplicates)
            {
                DataRow first = group.First();
                int sum = group.Sum(r => r.Field<int>("Position"));
                first.SetField("Position", sum);
                foreach (DataRow row in group.Skip(1))
                    dtPositions.Rows.Remove(row);
            }

            //return values
            WordsData.Set(context, dt);
            WordsPositions.Set(context, dtPositions);
        }

        //count occurrences of string in text
        public static int CountStringOccurr(string[] textArr, string pattern)
        {
            int count = 0;

            foreach (string word in textArr)
            {
                if(word == pattern)
                {
                    count++;
                }
            }

            return count;
        }

    }
    */

    public class Save_Attachment_By_Index : CodeActivity
    {
        [Category("Input")]
        [Description("EWS EmailMessage")]
        [DisplayName("EmailMessage")]
        [RequiredArgument]
        public InArgument<EmailMessage> Mail { get; set; }

        [Category("Input")]
        [Description("Folder path where attachment should be saved")]
        [DisplayName("Target directory path")]
        [RequiredArgument]
        public InArgument<string> SaveTo { get; set; }

        [Category("Input")]
        [Description("Attachment index")]
        [DisplayName("Index")]
        [RequiredArgument]
        public InArgument<int> AttachIndex { get; set; }

        [Category("Optional")]
        [Description("Extensions you want to save, separate extensions by | i.e: 'pdf|PDF' for .pdf files.")]
        [DisplayName("Filter")]
        public InArgument<string> Extensions { get; set; }

        [Category("Output")]
        [DisplayName("FileName")]
        [Description("Each file is renamed when saving, return string with new (unique) file name.")]
        [RequiredArgument]
        public OutArgument<string> FileName { get; set; }

        public Save_Attachment_By_Index()
        {
            //define default vars
            this.SaveTo = new InArgument<string>(@"C:\Users\Public\Documents\");
        }

        protected override void Execute(CodeActivityContext context)
        {
            //ExchangeService exchange = ExchangeService.Get(context);
            EmailMessage msg = Mail.Get(context);
            msg.Load();

            string path = SaveTo.Get(context);
            string pattern = Extensions.Get(context);
            int attachIndex = AttachIndex.Get(context);
            Attachment attach;

            if (msg.HasAttachments)
            {
                attach = msg.Attachments[attachIndex];

                //there is not filter, save all
                if (pattern == null)
                {
                    FileName.Set(context, SaveFile(attach, path));
                }
                else
                {
                    string[] arrPatterns = pattern.Split('|');
                    //iterate through filters
                    foreach (string p in arrPatterns)
                    {
                        if (attach.Name.EndsWith(p))
                        {
                            FileName.Set(context, SaveFile(attach, path));
                        }
                    }
                }
            }
            else { Console.WriteLine("Save_Attachment: nothing attached."); }
        }

        public string SaveFile(Attachment attach, string path)
        {
            string saveName = null;

            if (attach is FileAttachment) //file
            {
                FileAttachment fileAttachment = attach as FileAttachment;
                fileAttachment.Load();

                //generate random name for file
                saveName = Path.GetRandomFileName().Replace(".", string.Empty) + Path.GetExtension(fileAttachment.Name.ToLower());

                fileAttachment.Load(@path + saveName);
                try
                {
                    fileAttachment.Load(@path + saveName);
                }
                catch (Exception e)
                {
                    throw new Exception(string.Format("Cannot write the file {0} to {1}", saveName, @path), e);
                }
            }
            else if (attach is ItemAttachment) //email's
            {
                ItemAttachment itemAttachment = attach as ItemAttachment;
                itemAttachment.Load(EmailMessageSchema.MimeContent);

                string fileExtension = "";

                //by default ItemAttachment has not extension, so have to handle it
                if (itemAttachment.Item is EmailMessage)
                {
                    fileExtension = ".eml";
                }
                else if (itemAttachment.Item is Contact)
                {
                    fileExtension = ".vcf";
                }
                else if (itemAttachment.Item is Appointment)
                {
                    fileExtension = ".ics";
                }
                else //just in case, if null or return empty string and accidentally there will be some extension!
                {
                    fileExtension = Path.GetExtension(itemAttachment.Name.ToLower());
                }

                //always rename file using unique name
                saveName = Path.GetRandomFileName().Replace(".", string.Empty);
                try
                {
                    File.WriteAllBytes(@path + saveName + fileExtension, itemAttachment.Item.MimeContent.Content);
                    saveName = saveName + fileExtension;
                }
                catch(Exception e)
                {
                    throw new Exception(string.Format("Cannot write the file {0} to {1}", saveName + fileExtension, @path), e);
                }
            }

            return saveName;
        }
    }
  
}
